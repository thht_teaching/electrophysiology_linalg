update_env:
	mamba env update -f environment.yml -p ./.venv

make_scene_draft:
	manim linalg.py -ql

make_scene_final:
	manim -qh linalg.py LinearAlgebra SVD

make_scene_gif:
	manim linalg.py -qh -i
