from manim import *


class MyNumberPlane(NumberPlane):
    def __init__(self, *args, center_point=ORIGIN, **kwargs):
        super().__init__(*args, **kwargs)
        self.shift(center_point)
