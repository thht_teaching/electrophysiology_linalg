import numpy as np
from manim import *


class NumpyVector(Matrix):
    def __init__(self, vector, *args, **kwargs):
        if vector.ndim == 1:
            vector = vector[:, np.newaxis]
        super().__init__(vector, *args, **kwargs)