import math
from copy import deepcopy

import PIL
from extra.objects.matrix import NumpyVector
from extra.objects.numberplane import MyNumberPlane
from manim import *
import sys
import os
import numpy as np
import array_to_latex as a2l
from thht_manim.camera import FixedThreeDCamera
from thht_manim.coordinate_systems import ThreeDSystem, BetterNumberPlane
from thht_manim.matrix import MatrixTracker, TrackerVectorWithFormulaAndArrow, MatrixVariable, RotationMatrixVariable, \
    ImageFromVectorAlignedToMatrixAsRect, ImageFromVector, MatrixAsRect
from thht_manim.numbers import MathTexWithOpaqueBackground

# TODO before next time:
# slide 53: wrong number in formula
# improve explanation of cov

sys.path.append(os.getcwd())

from thht_manim.scene import AddSlideNumber, SlideScene, \
    AddExtraFrame, SplitScene
from thht_manim.images import PNGImage, JPGImage
from thht_manim.slides import SlideHeader, SlideText, SlideTextMarkdown

tex_template = TexFontTemplates.droid_sans

tex_template.preamble = tex_template.preamble.replace('\\usepackage[LGRgreek]{mathastext}', '')

tex_template.add_to_preamble(
    r'''
\usepackage{enumitem}
\usepackage{xfrac}
\usepackage[none]{hyphenat}
\usepackage{commath}
\usepackage{tabto}
\setlist{topsep=0pt,labelindent=\parindent,leftmargin=*}
    '''
)

config['tex_template'] = tex_template
config['max_files_cached'] = 1000
#config['frame_rate'] = 2


class LinearAlgebra(SlideScene, AddSlideNumber, SplitScene, AddExtraFrame, Scene):
    big_math_kwargs = dict(
        tex_environment='align*',
        markdown=False,
        scale_factor=1.4,
        to_header_buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * 3,
        center_align=True
    )

    small_math_kwargs = deepcopy(big_math_kwargs)
    small_math_kwargs['scale_factor'] = 0.8

    number_plane_config = dict(
        center_point=ORIGIN - 0.5*UP,
        y_range=[-3.5, 3.5, 1],
        axis_config={
            'include_ticks': True
        }
    )

    a2l_kwargs = dict(
        row=False,
        print_out=False,
        frmt='{:d}'
    )

    float_a2l_kwargs = dict(row=False,
                            print_out=False,
                            frmt='{:.2f}')
    scale_astronaut = 2
    zoomed_dim = (4, 6)
    scale_factor_width_vectors = 20

    CONFIG = {
        'camera_class': FixedThreeDCamera,
        'cut_axes_at_radius': True,
        "camera_config": {"should_apply_shading": True,
                          "exponential_projection": True},
    }

    three_d_axes_config = {
        'x_range': [-3.5, 3.5, 1],
        'y_range': [-3.5, 3.5, 1],
        'z_range': [-3.5, 3.5, 1],
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sbdl_logo = PNGImage(file_name='sbdl_logo')
        self.sbdl_logo.scale(0.06)
        self.sbdl_logo.to_corner(UP + LEFT, buff=DEFAULT_MOBJECT_TO_EDGE_BUFFER / 2)
        self.add(self.sbdl_logo)

        self.plus_logo = PNGImage(file_name='plus_logo')
        self.plus_logo.match_height(self.sbdl_logo)
        self.plus_logo.to_corner(UP + RIGHT, buff=DEFAULT_MOBJECT_TO_EDGE_BUFFER / 2)
        self.add(self.plus_logo)

        self.to_remove = VGroup()

    def clear(self):
        super().clear()
        self.add(self.sbdl_logo, self.plus_logo)

    def part01_title_page(self):
        title_text = SlideHeader('Linear Algebra')
        title_text.set_y(2)
        subtitle_text = Tex('Vectors, Matrices and more\\\\Thomas Hartmann')
        self.add(title_text, subtitle_text)

        self.wait()

    def part02_overview(self):
        self.clear()
        self.set_header('Recap from last week')
        self.set_text(r'''
* Complex numbers are a great tool to represent 2D objects
* We can keep using all our operations and rules        
''')

        self.wait()

        self.clear()
        self.set_header('Can we go further?')
        self.set_text(r'''
* But there are still restrictions:
    * Only 2 dimensions. In M/EEG, each channel is a dimension (306 in our case!)
    * We want to factor in time as well
    * We can only scale uniformly
    * The axes are always cartesian
''')

        self.wait()

        self.clear()
        self.set_header('Adding dimension = adding more strange numbers?')
        self.set_text(r'''
* We already know how to get from 1D to 2D:
    * Find a something that is not on the original number space (line, plane...)
    * Give it a fancy letter (like $j$...)
    * Now we have: $(a + bi + cj + \dots)$
    * Calculate happily ever after...       
''')

        self.wait()

    def part03_introduce_hamilton(self):
        self.clear()
        self.set_header('Sir William Rowan Hamilton tried for three dimensions...')

        hamilton = PNGImage(file_name='hamilton', height=4)
        hamilton.next_to(self._slide_header, DOWN, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)
        self.add(hamilton)

        self.wait()

        self.set_header('But here is the problem:')

        self.set_text(r'''
You have to figure out how to multiply all\newline combinations of your letters:

* $ij=\mathord{?}$
* $i^2=\mathord{?}$
* $j^2=\mathord{?}$      
''',
                      alignment='')

        self._slide_text.to_edge(RIGHT, 2*DEFAULT_MOBJECT_TO_EDGE_BUFFER)
        self._slide_text.match_y(hamilton, UP)

        self.play(
            AnimationGroup(
                ApplyMethod(hamilton.to_edge, LEFT, 2*DEFAULT_MOBJECT_TO_EDGE_BUFFER),
                FadeIn(self._slide_text)
            )
        )

        self.set_header('So Sir William took a walk')
        hamilton_plaque = JPGImage(file_name='hamilton_plaque', height=4)
        hamilton_plaque.match_y(hamilton, UP)
        hamilton_plaque.to_edge(RIGHT, 2*DEFAULT_MOBJECT_TO_EDGE_BUFFER)

        caption_text = SlideText('“Nor could I resist the impulse – unphilosophical as it may have been – to cut with a knife on a stone of Brougham Bridge as we passed it, the fundamental formula”')

        caption_text.center()
        caption_text.next_to(hamilton, DOWN, coor_mask=[0, 1, 0])

        self.play(
            FadeIn(hamilton_plaque),
            FadeOut(self._slide_text),
            Write(caption_text)
        )

        self.clear()
        self.set_header('Rules for quarternions')

        self.set_text(r'''
\begin{itemize}
\item $a + bi + cj + dk$
\item $i, j, k$: Unit vectors (i.e. length=1) pointing towards the respective axis
\item $i^2 = j^2 = k^2 = ijk = -1$
\item \begin{tabular}{|l|l|} \hline
$ij=k$ & $ji=-k$ \\ \hline
$jk=i$ & $kj=-i$ \\ \hline
$ki=j$ & $ik=-j$ \\ \hline
\end{tabular}
\end{itemize}
''', markdown=False)

        self.wait()

        self.clear()
        self.set_header('Want to add more letters?')

        hurwitz = JPGImage(file_name='hurwitz').scale_to_fit_height(4)
        hurwitz.center()

        self.set_text('Adolf Hurwitz proved that this is only possible for $2^n$ dimensions.',
                      max_width='15em')
        self._slide_text.center()
        self._slide_text.next_to(hurwitz)
        Group(hurwitz, self._slide_text).center()

        self.add(hurwitz)

        self.wait()
        self.clear()

        self.set_header('And people became a bit annoyed')

        self.set_text(r'''
"Quaternions came from Hamilton after his really good work had been done; and, though beautifully ingenious, have been an unmixed evil to those who have touched them in any way, including Clerk Maxwell." 

W. Thompson, Lord Kelvin (1892)
''')
        self.wait()

        self.set_text(r'''
"I came later to see that, as far as the vector analysis I required was concerned, the quaternion was not only not required, but was a positive evil of no inconsiderable magnitude; and that by its avoidance the establishment of vector analysis was made quite simple and its working also simplified, and that it could be conveniently harmonised with ordinary Cartesian work." 

Oliver Heaviside (1893)
''')

        self.wait()

        self.set_text(r'''
"I regard it as an inelegance, or imperfection, in quaternions, or rather in the state to which it has been hitherto unfolded, whenever it becomes or seems to become necessary to have recourse to x, y, z, etc."

William Rowan Hamilton        
''')

        self.wait()

    def part04_introduce_vectors(self):
        self.clear()
        self.set_header('Vectors')

        v1 = np.array([2, 1])

        coord_system = MyNumberPlane(**self.number_plane_config)
        coord_system.remove(coord_system.background_lines)
        coord_system.add_coordinates()
        v1_arrow = coord_system.get_vector(v1)

        v1_mat = NumpyVector(v1)
        v1_mat.next_to(v1_arrow.tip, UP + RIGHT)

        self.play(Create(coord_system), add_split=False)
        self.play(Create(v1_arrow), add_split=False)
        self.play(Create(v1_mat), add_split=False)

        v1_x_arrow = coord_system.get_vector([v1[0], 0], color=RED)
        v1_y_arrow = coord_system.get_vector([0, v1[1]], color=GREEN)
        v1_y_arrow.set_x(coord_system.c2p(v1[0], 0)[0])

        self.play(
            AnimationGroup(
                Create(v1_x_arrow),
                FadeToColor(v1_mat.get_mob_matrix()[0][0], RED)
            ), add_split=False)
        self.play(
            AnimationGroup(
                Create(v1_y_arrow),
                FadeToColor(v1_mat.get_mob_matrix()[1][0], GREEN)
            )
        )

    def part05_3d_vectors(self):
        self.clear()
        self.set_header('3d Vectors')

        v1 = np.array([-2, 3, 3])

        coord_system = ThreeDSystem(**self.three_d_axes_config)
        coord_system.add_coordinates()
        coord_system.scale(0.8)

        self.add(coord_system)

        rotation = rotation_matrix(0.125 * math.pi, RIGHT) @ rotation_matrix(0.125*math.pi, UP)

        self.play(
            ApplyMethod(
                coord_system.apply_matrix, rotation, {'about_point': coord_system.c2p(0, 0, 0)},
                run_time=2
            ),
        )

        v1_object = coord_system.get_vector(v1)
        v1_mat = NumpyVector(v1)
        v1_mat.shift(np.array([4, 1, 0]))

        coord_system.add(v1_object)

        self.play(
            Create(v1_object),
            FadeIn(v1_mat),
            add_split=False
        )

        v1_x_object = coord_system.get_vector([v1[0], 0, 0], color=RED)
        v1_y_object = coord_system.get_vector([v1[0], v1[1], 0], start_coords=[v1[0], 0, 0], color=GREEN)
        v1_z_object = coord_system.get_vector(v1, start_coords=[v1[0], v1[1], 0], color=BLUE)

        self.play(
            AnimationGroup(
                Create(v1_x_object),
                FadeToColor(v1_mat.get_mob_matrix()[0][0], RED)
            ), add_split=False)
        self.play(
            AnimationGroup(
                Create(v1_y_object),
                FadeToColor(v1_mat.get_mob_matrix()[1][0], GREEN)
            ), add_split=False)
        self.play(
            AnimationGroup(
                Create(v1_z_object),
                FadeToColor(v1_mat.get_mob_matrix()[2][0], BLUE)
            ),
        )

    def part06_about_dimensions(self):
        self.clear()
        self.set_header('How to add dimensions')
        self.set_text(r'''
* Vectors can have any number of dimensions
* Just add more numbers to them
* It does not change how a vector is operated
    ''')

        v1 = np.array([5, 3, 6, 7, 2, 30])

        v1_object = NumpyVector(v1).to_edge(RIGHT, buff=2*DEFAULT_MOBJECT_TO_EDGE_BUFFER)

        self.play(
            AnimationGroup(
                Create(v1_object.get_brackets()),
                Create(v1_object.get_entries()[:2]),
                run_time=1), add_split=False)

        for entry in v1_object.get_entries()[2:]:
            self.play(Create(entry, run_time=0.5), add_split=False)

        self.wait()

        self.set_text('Dimensions higher than 3 are hard to interpret as spatial.\n\n'
                      'But if you measure something with $n$ sensors, you measure it in $n$ dimensions:',
                      alignment='', max_width='25em')

        meg_data_image = PNGImage(file_name='meg_data').scale_to_fit_height(4)
        meg_data_image.next_to(self._slide_text, DOWN)
        fmri_image = JPGImage(file_name='fmri').scale_to_fit_height(4)
        fmri_image.next_to(self._slide_text, DOWN)

        self.add(meg_data_image)

        self.play(FadeIn(meg_data_image))
        self.play(
            FadeOut(meg_data_image),
            FadeIn(fmri_image)
        )

    def part07_basic_vector_operations(self):
        self.clear()
        self.set_header('What can we do with vectors?')

        coord_system = BetterNumberPlane(**self.number_plane_config)

        v1 = np.array([3, 1])
        v2 = np.array([1, 2])
        add_result = v1 + v2

        add_formula = MathTexWithOpaqueBackground(
            a2l.to_ltx(v1, **self.a2l_kwargs), '+',
            a2l.to_ltx(v2, **self.a2l_kwargs), '=',
            a2l.to_ltx(add_result, **self.a2l_kwargs),
            tex_to_color_map={
                a2l.to_ltx(v1, **self.a2l_kwargs): RED,
                a2l.to_ltx(v2, **self.a2l_kwargs): GREEN,
                a2l.to_ltx(add_result, **self.a2l_kwargs): BLUE,
            }
        ).scale(1.3)

        v1_object = coord_system.get_vector(v1, color=RED)
        v2_object = coord_system.get_vector(v2, color=GREEN)
        add_result_object = coord_system.get_vector(add_result, color=BLUE)

        add_formula.next_to(self.sbdl_logo, DOWN, aligned_edge=LEFT, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        self.play(Create(coord_system), add_split=False)
        self.play(
            AnimationGroup(
                FadeIn(add_formula.background),
                Create(v1_object),
                Write(add_formula.tex[0])
            ), add_split=False)
        self.play(
            AnimationGroup(
                Create(v2_object),
                Write(add_formula.tex[2])
            )
        )

        self.set_header('Addition')

        self.play(
            AnimationGroup(
                Write(add_formula.tex[1]),
                ApplyMethod(v2_object.shift, coord_system.c2p(*v1) - coord_system.c2p(0, 0, 0))
            ), add_split=False)
        self.play(
            AnimationGroup(
                Write(add_formula.tex[3:]),
                Create(add_result_object)
            )
        )

        scale_factor = 2
        scale_result = scale_factor * v1
        scale_result_object = coord_system.get_vector(scale_result, color=BLUE)

        scale_formula = MathTexWithOpaqueBackground(
            str(scale_factor),
            a2l.to_ltx(v1, **self.a2l_kwargs), '=',
            a2l.to_ltx(scale_result, **self.a2l_kwargs),
            tex_to_color_map={
                a2l.to_ltx(v1, **self.a2l_kwargs): RED,
                a2l.to_ltx(scale_result, **self.a2l_kwargs): BLUE
            }
        ).scale(1.3)

        scale_formula.align_to(add_formula, UP + LEFT)

        self.set_header('Scalar Multiplication')

        self.play(
            FadeOut(
                Group(add_formula, add_result_object, v2_object)
            ), add_split=False)
        self.play(
            AnimationGroup(
                FadeIn(scale_formula.background),
                Write(scale_formula.tex[1])
            ), add_split=False)
        self.play(Write(scale_formula.tex[0]))

        self.play(
            ReplacementTransform(v1_object, scale_result_object),
            Write(scale_formula.tex[2:])
        )

        self.set_header('Calculate the Norm')

        norm_formula = MathTexWithOpaqueBackground(
            '\\abs{' + a2l.to_ltx(scale_result, **self.a2l_kwargs) + '}',
            '=', '\\sqrt{%d^2 + %d^2}' % (scale_result[0], scale_result[1]),
            '=', '%.3f' % (np.linalg.norm(scale_result), ),
            )

        norm_formula.align_to(scale_formula, LEFT+UP)

        self.play(ReplacementTransform(scale_formula, norm_formula))

    def part08_the_dot_product(self):
        self.clear()
        self.set_header('The dot product')

        angles_of_interest = [1/4*math.pi,
                              1/2*math.pi,
                              math.pi,
                              2*math.pi]

        kwargs = deepcopy(self.number_plane_config)
        kwargs['y_range'] = [-1.25, 1.25, 1]
        kwargs['center_point'] = ORIGIN - 1.5 * UP
        coord_system = BetterNumberPlane(**kwargs).scale(2)

        self.add(coord_system)

        v1 = np.array([1, 0])
        v2 = np.array([0.8, 0])

        v1_object = coord_system.get_vector(v1, color=RED)
        v2_object = coord_system.get_vector(v2, color=GREEN)
        result_object = coord_system.get_vector(v2, color=BLUE)
        projection_object = Line(coord_system.c2p(0, 0, 0), coord_system.c2p(*v2), color=WHITE)

        angle_tracker = ValueTracker(0)

        v2_object.add_updater(
            lambda v: v.set_angle(angle_tracker.get_value())
        )

        v1_object.add_updater(
            lambda v: v.put_start_and_end_on(coord_system.c2p(0, 0, 0), coord_system.c2p(v1[0], v1[1], 0))
        )

        def formula_updater(f):
            rot = rotation_matrix(angle_tracker.get_value(), axis=OUT)

            new_v2 = rot[0:2, 0:2] @ v2
            new_v2_formula = SingleStringMathTex(
                a2l.to_ltx(new_v2, row=False,
                           print_out=False,
                           frmt='{:.2f}')
            ).scale(1.3)

            new_v2_formula.align_to(f.submobjects[2], UP+LEFT)
            new_v2_formula.match_color(f.submobjects[2])

            old_family = f.get_family()
            old_mo = f.submobjects[2]
            f.submobjects[2] = new_v2_formula

            def remove_old_mo(mo):
                if mo in old_family:
                    old_mo.points[:] = 0
                    for m in old_mo.get_family():
                        m.points[:] = 0
                    self.remove(*old_mo.get_family())
                    old_mo.submobjects = []

            remove_old_mo(old_mo)

            new_result = np.dot(v1, new_v2)
            v1_unit = v1 / np.linalg.norm(v1)
            result_end = new_result * v1_unit
            result_end = np.append(result_end, [0])
            result_end_converted = coord_system.c2p(*result_end)
            start = coord_system.c2p(0, 0, 0)

            if not np.all(start == result_end_converted):
                result_object.put_start_and_end_on(start, result_end_converted)

            start = v2_object.get_end()
            end = result_end / np.linalg.norm(v1)
            end = coord_system.c2p(*end)

            if not np.all(start == end):
                try:
                    projection_object.start = start
                    projection_object.end = end
                    projection_object.set_points_as_corners([start, end])
                    projection_object.reset_points()
                    projection_object.generate_points()
                except:
                    pass

            new_result_tex = SingleStringMathTex('%.2f' % (new_result, )).scale(1.3)
            new_result_tex.align_to((f.submobjects[4]), UP+LEFT)
            old_mo = f.submobjects[4]
            f.submobjects[4] = new_result_tex

            remove_old_mo(old_mo)

        def make_formula():
            f = MathTexWithOpaqueBackground(
                a2l.to_ltx(v1, **self.a2l_kwargs), '\\cdot',
                a2l.to_ltx(v2, row=False,
                           print_out=False,
                           frmt='{:.2f}'), '\\quad=\\quad',
                '?',
                tex_to_color_map={
                    a2l.to_ltx(v1, **self.a2l_kwargs): RED,
                    a2l.to_ltx(v2, row=False,
                               print_out=False,
                               frmt='{:.2f}'): GREEN
                },
                padding={'right': 1}
            ).scale(1.3)

            f.tex.add_updater(formula_updater)

            f.next_to(self.sbdl_logo, DOWN, aligned_edge=LEFT,
                      buff=2 * DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

            return f

        formula = make_formula()

        self.play(
            Create(v1_object),
            Create(v2_object),
            Create(result_object),
            Create(projection_object),
            Write(formula),
            ApplyMethod(angle_tracker.set_value, 0)
        )

        for cur_angle in angles_of_interest:
            self.play(
                ApplyMethod(angle_tracker.set_value, cur_angle, run_time=3)
            )

        v1 = 2 * v1

        old_formula = formula
        formula = make_formula()

        self.play(
            ReplacementTransform(old_formula, formula),
        )
        angle_tracker.set_value(0)

        for cur_angle in angles_of_interest:
            self.play(
                ApplyMethod(angle_tracker.set_value, cur_angle, run_time=3)
            )

        v1[1] = -1

        old_formula = formula
        formula = make_formula()

        self.play(
            ReplacementTransform(old_formula, formula),
        )
        angle_tracker.set_value(0)

        for cur_angle in angles_of_interest:
            self.play(
                ApplyMethod(angle_tracker.set_value, cur_angle, run_time=3)
            )

    def part09_the_dot_product_part2(self):
        self.clear()
        self.set_header('The dot product')
        self.set_text(r'''
* $\begin{bmatrix}
  x_1\\\\
  x_2
  \end{bmatrix}
  \cdot
  \begin{bmatrix}
  y_2\\\\
  y_2
  \end{bmatrix}
  = x_1 * y_1 + x_2 * y_2$
* $\begin{bmatrix}
  x_1\\\\
  x_2
  \end{bmatrix}
  \cdot
  \begin{bmatrix}
  y_2\\\\
  y_2
  \end{bmatrix}
  = \abs{\vec{x}} \abs{\vec{y}} * \cos \sphericalangle (\vec{x}, \vec{y})$
* Project $\vec{y}$ on $\vec{x}$ and multiply it with the length of $\vec{x}$
* Or the other way around
* **Important**: If $\vec{x} \cdot \vec{y} = 0$, then $\vec{x}$ and $\vec{y}$ are orthogonal
''')

        self.wait()

    def part10_base_vectors(self):
        self.clear()
        self.set_header('How to define a coordinate system',
                        scale_factor=1.1,
                        max_width='20em')

        coordinate_system = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg.fade(0.5)

        v1 = MatrixTracker(np.array([2, 3]))
        v1_mat = MathTexWithOpaqueBackground(a2l.to_ltx(v1.get_value(), **self.float_a2l_kwargs),
                                             background_opacity=0.5)
        v1_arrow = coordinate_system.get_vector(v1.get_value(), color=BLUE)
        v1_formula01 = MathTexWithOpaqueBackground(
            r'\vec{v} = ', a2l.to_ltx(v1.get_value(), **self.float_a2l_kwargs),
            tex_to_color_map={
                r'\vec{v}': BLUE
            },
            background_opacity=0.5
        )

        v1_arrow.add_updater(
            lambda o: self._reposition_arrow(o, coordinate_system.c2p(0, 0), coordinate_system.c2p(*v1.get_value()))
        )

        v1_mat.add_updater(
            lambda o: o.next_to(v1_arrow.tip, RIGHT)
        )

        v1_formula01.next_to(self.sbdl_logo, DOWN, aligned_edge=LEFT, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        self.play(Create(coordinate_system), add_split=False)
        self.play(Create(v1_arrow), add_split=False)
        self.play(Write(v1_mat), add_split=False)
        self.play(Write(v1_formula01))

        self.add(coordinate_system_bg)

        img_perspective = JPGImage(file_name='perspective').scale_to_fit_height(6)

        self.play(
            FadeIn(img_perspective)
        )

        bg_rect = Rectangle(width=img_perspective.width,
                            height=img_perspective.height,
                            color=BLACK,
                            fill_color=BLACK,
                            fill_opacity=1)
        bg_rect.align_to(img_perspective, UP + LEFT)

        text = SlideTextMarkdown(r'''
* We always assumed that the first value points to the right and the second value points up
* We also assumed that the length of $1$ means the same for both directions         
''',
                                 max_width='23em')

        text.to_corner(bg_rect.get_corner(UP+LEFT))
        text.shift(np.array([0, -1, 0]))

        self.play(
            FadeOut(img_perspective),
            FadeIn(bg_rect),
            Write(text)
        )

        new_text = SlideTextMarkdown(r'''
* But there is a way to define the direction and the length of the axis:
* Meet "unit vectors":
    * Unit vectors have a length of $1$
    * So only their direction matters
* For each direction, we introduce one unit vector. This defines our coordinate system      
* These unit vectors are called "base vectors":
    * $\widehat{i}, \widehat{j}$\dots  
''',
                                     max_width='23em').scale(0.9)

        new_text.to_corner(bg_rect.get_corner(UP + LEFT))

        self.set_header('Base Vectors')
        self.play(ReplacementTransform(text, new_text))

        i_hat = MatrixTracker(np.array([1, 0]))
        j_hat = MatrixTracker(np.array([0, 1]))
        i_hat_arrow = coordinate_system_bg.get_vector(i_hat.get_value(), color=RED)
        j_hat_arrow = coordinate_system_bg.get_vector(j_hat.get_value(), color=GREEN)
        i_hat_tip_text = MathTex(r'\widehat{i}', color=RED)
        j_hat_tip_text = MathTex(r'\widehat{j}', color=GREEN)
        i_hat_formula = MathTexWithOpaqueBackground(
            r'\widehat{i} = ', a2l.to_ltx(i_hat.get_value(), **self.float_a2l_kwargs),
            color=RED,
            background_opacity=0.5,
        )
        j_hat_formula = MathTexWithOpaqueBackground(
            r'\widehat{j} = ', a2l.to_ltx(j_hat.get_value(), **self.float_a2l_kwargs),
            color=GREEN,
            background_opacity=0.5,
        )

        i_hat_tip_text.next_to(i_hat_arrow.tip, UP+RIGHT)
        j_hat_tip_text.next_to(j_hat_arrow.tip, UP + RIGHT)

        j_hat_formula.to_corner(DOWN + LEFT)
        i_hat_formula.next_to(j_hat_formula, UP, aligned_edge=LEFT)

        i_hat_arrow.add_updater(
            lambda o: self._reposition_arrow(o, coordinate_system_bg.c2p(0, 0, 0), coordinate_system_bg.c2p(*np.append(i_hat.get_value(), [0])))
        )

        j_hat_arrow.add_updater(
            lambda o: self._reposition_arrow(o, coordinate_system_bg.c2p(0, 0, 0),
                                             coordinate_system_bg.c2p(
                                                 *np.append(j_hat.get_value(), [0])))
        )

        i_hat_tip_text.add_updater(
            lambda o: o.next_to(i_hat_arrow.tip, UP+RIGHT)
        )

        j_hat_tip_text.add_updater(
            lambda o: o.next_to(j_hat_arrow.tip, UP + RIGHT)
        )

        i_hat_formula.add_updater(
            lambda o: self._update_matrix_part(o, 1, i_hat.get_value(), a2l_kwargs=self.float_a2l_kwargs)
        )

        j_hat_formula.add_updater(
            lambda o: self._update_matrix_part(o, 1, j_hat.get_value(), a2l_kwargs=self.float_a2l_kwargs)
        )

        self.play(
            AnimationGroup(
                FadeOut(bg_rect),
                FadeOut(new_text)
            ), add_split=False)
        self.play(
            AnimationGroup(
                Create(i_hat_arrow),
                Write(i_hat_tip_text),
                Write(i_hat_formula)
            ), add_split=False)
        self.play(
            AnimationGroup(
                Create(j_hat_arrow),
                Write(j_hat_tip_text),
                Write(j_hat_formula)
            )
        )

        coordinate_system.add_updater(
            lambda o: o.set_i_hat_j_hat(i_hat.get_value(), j_hat.get_value())
        )

        v1_formula02 = MathTexWithOpaqueBackground(
            r'\vec{v} = ', str(v1.get_value()[0]), a2l.to_ltx(i_hat.get_value(), **self.float_a2l_kwargs), '+',
            str(v1.get_value()[1]), a2l.to_ltx(j_hat.get_value(), **self.float_a2l_kwargs),
            tex_to_color_map={
                r'\vec{v}': BLUE
            },
            background_opacity=0.5
        )
        v1_formula02.tex.submobjects[3].set_color(RED)
        v1_formula02.tex.submobjects[6].set_color(GREEN)

        v1_formula02.add_updater(
            lambda o: self._update_matrix_part(o, 3, i_hat.get_value(), a2l_kwargs=self.float_a2l_kwargs)
        )

        v1_formula02.add_updater(
            lambda o: self._update_matrix_part(o, 6, j_hat.get_value(),
                                               a2l_kwargs=self.float_a2l_kwargs)
        )

        v1_formula02.next_to(v1_formula01, DOWN, aligned_edge=LEFT)

        i_hat_v_arrow = coordinate_system.get_vector((v1.get_value()[0], 0), color=RED)
        j_hat_v_arrow = coordinate_system.get_vector(v1.get_value(), start_coords=(v1.get_value()[0], 0), color=GREEN)

        i_hat_v_arrow.add_updater(
            lambda o: self._reposition_arrow(o, coordinate_system.c2p(0, 0), coordinate_system.c2p(*(v1.get_value()[0], 0)))
        )

        j_hat_v_arrow.add_updater(
            lambda o: self._reposition_arrow(o, coordinate_system.c2p(*(v1.get_value()[0], 0)), coordinate_system.c2p(*v1.get_value()))
        )

        self.play(
            AnimationGroup(
                Write(v1_formula02),
                Create(i_hat_v_arrow)
            ), add_split=False)
        self.play(Create(j_hat_v_arrow))

        self.play(
            AnimationGroup(
                FadeOut(VGroup(i_hat_v_arrow, j_hat_v_arrow))
            ), add_split=False)
        self.play(ApplyMethod(i_hat.set_single_value, 0, 2))

        self.play(
            ApplyMethod(j_hat.set_single_value, 0, -1)
        )

        self.play(
            ApplyMethod(j_hat.set_single_value, 1, -1)
        )

        self.play(
            ApplyMethod(i_hat.set_value, np.array([1, 0])),
            ApplyMethod(j_hat.set_value, np.array([0, 1]))
        )

        angle = 60*DEGREES

        self.play(
            ApplyMethod(i_hat.set_value, np.array([math.cos(angle), math.sin(angle)])),
            ApplyMethod(j_hat.set_value, np.array([-math.sin(angle), math.cos(angle)]))
        )

        self.remove(self._slide_number_object)

        self.saved_state = self.mobjects
        self.trackers = {
            'i_hat': i_hat,
            'j_hat': j_hat,
            'v1': v1
        }
        self.stuff = {
            'i_hat_formula': i_hat_formula,
            'j_hat_formula': j_hat_formula,
            'v1_formula01': v1_formula01,
            'v1_formula02': v1_formula02,
            'i_hat_arrow': i_hat_arrow,
            'j_hat_arrow': j_hat_arrow,
            'v1_arrow': v1_arrow,
            'coordinate_system': coordinate_system,
            'coordinate_system_bg': coordinate_system_bg,
            'i_hat_tip_text': i_hat_tip_text,
            'j_hat_tip_text': j_hat_tip_text
        }

        self.clear()
        self.set_header('Recap')
        self.set_text('In order to transform any vector, you just need to know where '
                      'your base vectors need to end up! \\\\ \\bigskip '
                      'Each coordinate becomes a linear combination of '
                      'the original vector and the respective base vector.',
                      markdown=False)

        self.wait()
        self.clear()

    def part12_linear_dependence(self):
        self.clear()

        self.play(
            FadeIn(Group(*self.saved_state)),
            add_split=False
        )

        self.play(
            ApplyMethod(self.trackers['i_hat'].set_value, np.array([1, 0])),
            ApplyMethod(self.trackers['j_hat'].set_value, np.array([0, 1]))
        )

        self.play(
            ApplyMethod(self.trackers['i_hat'].set_value, np.array([2, 1]))
        )

        self.play(
            ApplyMethod(self.trackers['j_hat'].set_value, np.array([4, 2]))
        )

        to_fade_out = [v for k, v in self.stuff.items() if k not in ['i_hat_formula', 'j_hat_formula']]

        def move_formulas(g):
            g[0].next_to(g[1], LEFT, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)
            g.next_to(self._slide_header, DOWN, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

            return g

        text = SlideTextMarkdown(r'''
* Note how $\hat{i}$ and $\hat{j}$ are just scaled versions of each other.
* This means they both point in the same direction.
* Remember that defining a vector $\vec{v}$ means scaling $\hat{i}$ and $\hat{j}$.
* $\vec{v}$ can thus only be on **the line** on which both $\hat{i}$ and $\hat{j}$ are.
* So, we basically reduced the number of dimensions by one.
* Note that this is not reversible!          
''')

        text.next_to(self._slide_header, DOWN, buff=10*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        self.play(
            FadeOut(Group(*to_fade_out)),
            ApplyFunction(move_formulas, Group(self.stuff['i_hat_formula'], self.stuff['j_hat_formula'])),
            Write(text)
        )

    def part13_introduce_matrix_multiplication(self):
        self.clear()
        self.set_header('Linear combinations and linear transforms')

        text1 = MathTex(r'\vec{vt} = ', r'v_1\begin{bmatrix}\widehat{i}_1\\ \widehat{i}_2\end{bmatrix} + v_2\begin{bmatrix}\widehat{j}_1\\ \widehat{j}_2\end{bmatrix} \\')
        text1.shift(np.array([0, -2, 0]))
        text2 = MathTex(r'\vec{vt} = ', r'{{ \begin{bmatrix}v_1\widehat{i}_1 + v_2\widehat{j}_1\\ v_1\widehat{i}_2 + v_2\widehat{j}_2\end{bmatrix} }}')
        text2.shift(np.array([0, -2, 0]))

        self.play(Write(text1))

        self.play(TransformMatchingShapes(text1, text2))

        x_part = text2.get_part_by_tex(r'v_1\widehat{i}_1 + v_2\widehat{j}_1').copy()
        x_part.submobjects = x_part.submobjects[1:-12]
        y_part = text2.get_part_by_tex(r'v_1\widehat{i}_2 + v_2\widehat{j}_2').copy()
        y_part.submobjects = y_part.submobjects[12:-2]

        def position_parts(g):
            g[0].shift(np.array([0, 3, 0]))
            g[0].to_edge(LEFT)
            g[1].next_to(g[0], DOWN, buff=3*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

            return g

        self.play(ApplyFunction(position_parts, VGroup(x_part, y_part)), add_split=False)

        x_brace = Brace(x_part, RIGHT)
        x_brace_text = x_brace.get_text('Linear combination for x')

        y_brace = Brace(y_part, RIGHT)
        y_brace_text = y_brace.get_text('Linear combination for y')

        self.play(
            Create(
                VGroup(x_brace, y_brace, x_brace_text, y_brace_text)
            ),
            add_split=False
        )

        def position_transform(g):
            g[0].to_edge(LEFT)
            brace = Brace(g[0], RIGHT)
            brace_text = brace.get_text('Linear Transform')

            g.add(brace, brace_text)

            return g

        self.play(
            ApplyFunction(position_transform, VGroup(text2)),
        )

        self.clear()
        self.add(text2)
        self.set_header('So, we have:')

        transform_vecs = MathTex(r'\widehat{i} = ', r'\begin{bmatrix}\widehat{i}_1\\\widehat{i}_2\end{bmatrix}',
                                 r'\widehat{j} = ', r'\begin{bmatrix}\widehat{j}_1\\\widehat{j}_2\end{bmatrix}')
        v_vec = MathTex(r'\vec{v} = \begin{bmatrix}v_1 \\ v_2\end{bmatrix}')

        v_vec.next_to(text2, UP, aligned_edge=LEFT)
        transform_vecs.next_to(v_vec, UP, aligned_edge=LEFT)

        self.play(
            Create(
                VGroup(transform_vecs, v_vec)
            )
        )

        transformation_mat = MathTex(r't = ', r'\begin{bmatrix}\widehat{i}_1 & \widehat{j}_1 \\ \widehat{i}_2 & \widehat{j}_2\end{bmatrix}')
        transformation_mat.next_to(v_vec, UP, aligned_edge=LEFT)

        self.play(TransformMatchingShapes(transform_vecs, transformation_mat))

        new_text2 = MathTex(r'\vec{vt} = ',
                            r'\begin{bmatrix}\widehat{i}_1 & \widehat{j}_1 \\ \widehat{i}_2 & \widehat{j}_2\end{bmatrix} * \begin{bmatrix}v_1 \\ v_2\end{bmatrix}',
                            r'=',
                            r'{{ \begin{bmatrix}v_1\widehat{i}_1 + v_2\widehat{j}_1\\ v_1\widehat{i}_2 + v_2\widehat{j}_2\end{bmatrix} }}')

        new_text2.match_x(text2, LEFT).match_y(text2)

        self.set_header('This is matrix multiplication:')

        self.play(TransformMatchingTex(text2, new_text2))

        self.clear()
        self.set_header('Matrix multiplication: Step by Step')

        t_mat = np.array([['a', 'b', 'c'], ['i', 'j', 'k']]).T
        t_matrix = Matrix(t_mat)

        v_mat = np.array([['u'], ['v']])
        v_matrix = Matrix(v_mat)

        final_mat = np.array(['au+iv', 'bu+jv', 'cu+kv'])
        final_matrix = Matrix(final_mat)
        formula = VGroup(t_matrix, MathTex('*'), v_matrix, MathTex('='), final_matrix)
        formula.arrange()

        formula.center()

        elements = final_matrix.submobjects[0]
        final_matrix.submobjects[0] = VGroup()

        self.play(Create(formula))

        x_element = v_matrix.submobjects[0][0].copy()
        y_element = v_matrix.submobjects[0][1].copy()

        def send_xy(g, target_g):
            buff = 0.25*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER
            g[0].next_to(target_g[0], RIGHT, buff=buff)
            g[1].next_to(target_g[1], RIGHT, buff=buff)
            g[1].match_y(g[0], UP)

            return g

        elements_per_row = len(elements) // t_mat.shape[0]
        for row in range(t_mat.shape[0]):
            el_start_idx = row * elements_per_row
            el_stop_idx = (row + 1) * elements_per_row
            start_idx = row*2
            end_idx = start_idx + 3
            self.play(
                ApplyFunction(
                    lambda g: send_xy(g, VGroup(*t_matrix.submobjects[0][start_idx:end_idx])),
                    VGroup(x_element, y_element)
                ), add_split=False)
            self.play(Create(elements[el_start_idx:el_stop_idx]))

        self.clear()
        self.set_header('Now with two columns...')

        t_mat = np.array([['a', 'b', 'c'], ['i', 'j', 'k']]).T
        t_matrix = Matrix(t_mat)

        v_mat = np.array([['u', 'x'], ['v', 'y']])
        v_matrix = Matrix(v_mat)

        final_mat = np.array([['au+iv', 'bu+jv', 'cu+kv'], ['ax+iy', 'bx+jy', 'cx+ky']]).T
        final_matrix = Matrix(final_mat, h_buff=2)
        formula = VGroup(t_matrix, MathTex('*'), v_matrix, MathTex('='), final_matrix)
        formula.arrange()

        formula.center()

        elements = final_matrix.submobjects[0]
        final_matrix.submobjects[0] = VGroup()

        self.play(Create(formula))

        def send_xy(g, target_g):
            buff = 0.25*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER
            g[0].next_to(target_g[0], RIGHT, buff=buff)
            g[1].next_to(target_g[1], RIGHT, buff=buff)
            g[1].match_y(g[0], UP)

            return g

        for col in range(t_mat.shape[1]):
            x_element = v_matrix.submobjects[0][col].copy()
            y_element = v_matrix.submobjects[0][col+2].copy()

            for row in range(t_mat.shape[0]):
                start_idx = row*2
                end_idx = start_idx + 2
                row_idx = row * 2 + col
                add_split = False
                if row == t_mat.shape[0]-1 and col == t_mat.shape[1]-1:
                    add_split = True

                self.play(
                    ApplyFunction(
                        lambda g: send_xy(g, VGroup(*t_matrix.submobjects[0][start_idx:end_idx])),
                        VGroup(x_element, y_element)
                    ), add_split=False)
                self.play(Create(elements[row_idx]), add_split=add_split)
            self.remove(x_element, y_element)

        self.clear()
        self.set_header('We forgot: This is a matrix:')

        m_text = MathTex('m').to_edge(LEFT)
        n_text = MathTex('n').to_edge(DOWN)

        def position_t_mat_and_labels(d):
            d['tmat'].center()
            d['m_text'].next_to(d['tmat'], LEFT)
            d['n_text'].next_to(d['tmat'], DOWN)

            return d

        self.play(
            ApplyFunction(position_t_mat_and_labels, VDict(
                {
                    'tmat': t_matrix,
                    'm_text': m_text,
                    'n_text': n_text
                }
            ))
        )

        self.clear()
        self.set_header('We cannot multiply any two matrices')


        first_mat_obj = self.create_labeled_empty_mat(3, 2, '_1')
        second_mat_obj = self.create_labeled_empty_mat(2, 4, '_2')
        result_mat_obj = self.create_labeled_empty_mat(3, 4, '_1', '_2')

        formula = VGroup(first_mat_obj, MathTex('*'), second_mat_obj, MathTex('='), result_mat_obj).arrange()
        formula.set_y(1)

        self.play(Write(formula))

        shape_formula = MathTex(r'(m_1, n_1) * (m_2, n_2) = (m_1, n_2)',
                                substrings_to_isolate=['m_1', 'n_1', 'm_2', 'n_2', '*', '='])
        shape_formula.next_to(formula, DOWN, buff=6*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        inner_brace = Brace(VGroup(shape_formula[3], shape_formula[7]), DOWN)
        inner_brace_text = inner_brace.get_text('Must be equal').scale(0.8)

        outer_brace = Brace(VGroup(shape_formula[1], shape_formula[9]), UP)
        outer_brace_text = outer_brace.get_text('Defines shape of result').scale(0.8)

        self.play(Write(shape_formula), add_split=False)
        self.play(Create(VGroup(inner_brace, inner_brace_text)), add_split=False)
        self.play(Create(VGroup(outer_brace, outer_brace_text)))

        self.set_header('This also means that the order matters')
        checkmark = MathTex(r'\checkmark').scale(1.5).set_color(GREEN)
        checkmark.next_to(inner_brace_text, RIGHT)

        self.play(
            FadeOut(VGroup(outer_brace, outer_brace_text)),
            ApplyMethod(VGroup(inner_brace, inner_brace_text).set_color, GREEN),
            Create(checkmark)
        )

        first_part = VGroup(shape_formula[:5])
        second_part = VGroup(shape_formula[6:11])

        first_movement_arc = ArcBetweenPoints(first_part.get_center(), second_part.get_center())
        second_movement_arc = ArcBetweenPoints(second_part.get_center(), first_part.get_center())

        formular_first_movement_arc = ArcBetweenPoints(formula[0].get_center(), formula[2].get_center())
        formular_second_movement_arc = ArcBetweenPoints(formula[2].get_center(), formula[0].get_center() + LEFT)

        cross = MathTex(r'\times').replace(checkmark).set_color(RED)

        self.play(
            MoveAlongPath(first_part, first_movement_arc),
            MoveAlongPath(second_part, second_movement_arc),
            MoveAlongPath(formula[0], formular_first_movement_arc),
            MoveAlongPath(formula[2], formular_second_movement_arc),
            ApplyMethod(VGroup(inner_brace, inner_brace_text).set_color, RED),
            ReplacementTransform(checkmark, cross),
            FadeOut(formula[4]),
            FadeOut(shape_formula[12:])
        )

    def part14_more_matmult(self):
        self.clear()
        self.set_header('How to interpret a Matrix multiplication')

        self.set_text(r'''
{\LARGE $A \cdot x = y$}
\vspace{2ex}

* Transform $x$ using new base vectors specified in $A$:
    * Rotate
    * Scale
    * Shear
* Solve linear equations system
* Calculate the weighted sum of $x$ using weights in $A$:
    * $x = $ Activity at M/EEG sensors, $y =$ Activity at sources
''')

        self.wait()

        self.clear()
        self.set_header('Transformations')

        coordinate_system = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg.fade(0.5)

        vector = TrackerVectorWithFormulaAndArrow(coordinate_system_bg,
                                                  np.array([2, 1]))
        vector2 = TrackerVectorWithFormulaAndArrow(coordinate_system_bg,
                                                   np.array([2, -1]),
                                                   label=r'\vec{w}')

        vector.formula.next_to(self.sbdl_logo, DOWN, aligned_edge=LEFT)
        vector2.formula.next_to(vector.formula, RIGHT)

        t_mat = MatrixVariable('T', np.eye(2))

        t_mat.next_to(vector.formula, DOWN, aligned_edge=LEFT)

        result = TrackerVectorWithFormulaAndArrow(
            coordinate_system_bg, vector.tracker.get_value(),
            label=r'T \vec{v}',
            color=RED
        )

        result2 = TrackerVectorWithFormulaAndArrow(
            coordinate_system_bg, vector2.tracker.get_value(),
            label=r'T \vec{w}',
            color=GREEN
        )

        result.formula.next_to(t_mat, DOWN, aligned_edge=LEFT)
        result2.formula.next_to(result.formula, DOWN, aligned_edge=LEFT)

        result.add_updater(
            lambda r: r.tracker.set_value(t_mat.tracker.get_value() @ vector.tracker.get_value())
        )

        result2.add_updater(
            lambda r: r.tracker.set_value(t_mat.tracker.get_value() @ vector2.tracker.get_value())
        )

        self.play(
            Create(
                VGroup(
                    coordinate_system,
                    coordinate_system_bg,
                    vector,
                    t_mat,
                    result,
                    vector2,
                    result2
                ),
                run_time=4
            )
        )

        self.set_header('Scaling')

        self.play(
            ApplyMethod(t_mat.tracker.set_single_value, 0, 2, run_time=2)
        )
        self.play(
            ApplyMethod(t_mat.tracker.set_single_value, 3, 2, run_time=2),
        )

        self.set_header('Shear')

        self.play(
            ApplyMethod(t_mat.tracker.set_single_value, 1, 1, run_time=2)
        )

        self.set_header('Rotation')

        self.play(
            ApplyMethod(t_mat.tracker.set_value, np.eye(2))
        )

        result.updaters = []
        result2.updaters = []
        rot_t_mat = RotationMatrixVariable('T', 0)
        rot_t_mat.next_to(vector.formula, DOWN, aligned_edge=LEFT)

        def update_r(r):
            r.tracker.set_value(rot_t_mat.tracker.get_value() @ vector.tracker.get_value())
            return r

        result.add_updater(update_r)

        def update_r2(r):
            r.tracker.set_value(rot_t_mat.tracker.get_value() @ vector2.tracker.get_value())
            return r

        result2.add_updater(update_r2)

        self.remove(t_mat)
        self.add(rot_t_mat)

        self.wait()

        self.remove(self._slide_number_object)
        saved_state = self.mobjects

        self.clear()

        self.set_header('Wait... cos, sin, -cos??')
        this_numberplane_config = deepcopy(self.number_plane_config)
        this_numberplane_config['y_range'] = [-1.5, 1.5, 1]
        this_numberplane_config['x_range'] = [-1.5, 1.5, 1]

        number_plane = BetterNumberPlane(**this_numberplane_config).scale(2)
        number_plane_bg = BetterNumberPlane(**this_numberplane_config).fade(0.5).scale(2)
        unit_circle = Circle(color=GREEN, radius=number_plane.get_x_unit_size(),
                             stroke_width=12)
        unit_circle.move_to(number_plane.get_center(), ORIGIN)
        self.play(
            Create(
                VGroup(
                    number_plane,
                    number_plane_bg
                )
            )
        )

        self.set_header('Back to the unit circle and unit vectors')
        i_hat = MatrixTracker(np.array([1, 0]))
        j_hat = MatrixTracker(np.array([0, 1]))
        i_hat_arrow = number_plane_bg.get_vector(i_hat.get_value(), color=RED)
        j_hat_arrow = number_plane_bg.get_vector(j_hat.get_value(), color=GREEN)
        i_hat_tip_text = MathTex(r'\widehat{i}', color=RED)
        j_hat_tip_text = MathTex(r'\widehat{j}', color=GREEN)
        i_hat_tip_text.next_to(i_hat_arrow.tip, UP+RIGHT)
        j_hat_tip_text.next_to(j_hat_arrow.tip, UP + RIGHT)

        i_hat_arrow.add_updater(
            lambda o: self._reposition_arrow(o, number_plane_bg.c2p(0, 0, 0), number_plane_bg.c2p(*np.append(i_hat.get_value(), [0])))
        )

        j_hat_arrow.add_updater(
            lambda o: self._reposition_arrow(o, number_plane_bg.c2p(0, 0, 0),
                                             number_plane_bg.c2p(
                                                 *np.append(j_hat.get_value(), [0])))
        )

        i_hat_tip_text.add_updater(
            lambda o: o.next_to(i_hat_arrow.tip, UP+RIGHT)
        )

        j_hat_tip_text.add_updater(
            lambda o: o.next_to(j_hat_arrow.tip, UP + RIGHT)
        )

        self.play(Create(unit_circle), add_split=False)
        self.play(Create(i_hat_arrow),
                  Create(i_hat_tip_text), add_split=False)
        self.play(Create(j_hat_arrow),
                  Create(j_hat_tip_text))

        self.remove(self._slide_number_object)
        self.remove(self._slide_header)
        saved_state2 = self.mobjects

        self.clear()

        self.set_header('Now remember')
        self.set_text('To figure out the transform matrix, we just need to '
                      'figure out, where $\\widehat{i}$ and $\\widehat{j}$ should end up.',
                      markdown=False, center_align=True)

        self.wait()

        self.clear()
        self.add(*saved_state2)
        self.set_header('We do not want to scale, so they will both remain on the unit circle...',
                        max_width='20em')

        self.wait()

        self.set_header('Let\'s solve this for $\\widehat{i}$ first...')
        angle = 60 * DEGREES
        new_ihat_coords = rotate_vector(i_hat.get_value(), angle)
        new_ihat_vector = number_plane_bg.get_vector(new_ihat_coords, color=RED)
        new_ihat_x_vector = number_plane_bg.get_vector((new_ihat_coords[0], 0), color=WHITE)
        new_ihat_y_vector = number_plane_bg.get_vector(new_ihat_coords,
                                                       start_coords=(new_ihat_coords[0], 0),
                                                       color=WHITE)

        self.play(
            FadeOut(j_hat_arrow),
            FadeOut(j_hat_tip_text),
            add_split=False
        )

        self.play(Create(new_ihat_vector), add_split=False)
        self.play(Create(new_ihat_x_vector), add_split=False)
        self.play(Create(new_ihat_y_vector))

        brace_x = MathTex('\\cos', font_size=30).next_to(new_ihat_x_vector, DOWN, buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER/3)
        brace_y = MathTex('\\sin', font_size=30).rotate_in_place(-90 * DEGREES).next_to(new_ihat_y_vector, RIGHT, buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER/3)

        self.play(Create(brace_x), add_split=False)
        self.play(Create(brace_y))

        new_ihat_formula = MathTex(r'''
\widehat{i} = \begin{bmatrix} \cos(\varphi)\\\sin(\varphi) \end{bmatrix}        
''', font_size=40).to_edge(LEFT).shift([0, 1, 0])

        self.play(Create(new_ihat_formula))

        self.set_header('Now for $\\widehat{j}$...')

        self.play(
            FadeOut(i_hat_arrow),
            FadeOut(i_hat_tip_text),
            add_split=False
        )

        self.play(
            FadeIn(j_hat_arrow),
            FadeIn(j_hat_tip_text),
            add_split=False
        )

        self.bring_to_front(new_ihat_x_vector)

        def rotate_arrows(a, to_color=WHITE):
            a.rotate(90 * DEGREES, about_point=number_plane_bg.get_center())
            a.set_color(to_color)

            return a

        self.play(
            ApplyFunction(rotate_arrows, new_ihat_x_vector),
            ApplyFunction(rotate_arrows, new_ihat_y_vector),
            ApplyFunction(lambda x: rotate_arrows(x, to_color=GREEN), new_ihat_vector),
            Rotate(brace_x, 90 * DEGREES,
                   about_point=number_plane_bg.get_center()),
            Rotate(brace_y, 90 * DEGREES,
                   about_point=number_plane_bg.get_center()),
            add_split=False
        )

        new_jhat_formula = MathTex(r'''
{{ \widehat{j} = }} \begin{bmatrix} \cos(\varphi + 90^{\circ}) \\ \sin(\varphi + 90^{\circ}) \end{bmatrix}
''', font_size=40).next_to(new_ihat_formula, DOWN).to_edge(LEFT)
        final_jhat_formula = MathTex(r'''
{{ \widehat{j} = }} \begin{bmatrix} -\sin(\varphi) \\ \cos(\varphi) \end{bmatrix}
''', font_size=40).next_to(new_ihat_formula, DOWN).to_edge(LEFT)

        self.play(FadeIn(new_jhat_formula), add_split=False)
        self.wait()

        self.set_header('Trigonometry rules tell us that:')
        self.play(TransformMatchingTex(new_jhat_formula, final_jhat_formula))

        self.set_header('This results in...')
        final_t_matrix = MathTex(r'''
T = \begin{bmatrix}
\cos(\varphi) & -\sin(\varphi) \\
\sin(\varphi) & \cos(\varphi)
\end{bmatrix}            
''', font_size=40).next_to(final_jhat_formula, DOWN).to_edge(LEFT)

        self.play(Write(final_t_matrix))

        self.clear()

        self.add(*saved_state)

        self.wait()

        angles = [45*DEGREES, 90*DEGREES, 180*DEGREES]

        for cur_angle in angles:
            self.play(
                ApplyMethod(rot_t_mat.angle_tracker.set_value, cur_angle, run_time=2)
            )

    def part15_chain_transformtations(self):
        self.clear()
        self.set_header('Transformations can be chained', max_width='17em', scale_factor=1)

        coordinate_system = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg.fade(0.5)

        vector = TrackerVectorWithFormulaAndArrow(coordinate_system_bg,
                                                  np.array([1, 1]))

        vector.formula.next_to(self.sbdl_logo, DOWN, aligned_edge=LEFT)

        t1 = np.eye(2)
        t1[0, 0] = 2
        t2 = rotation_matrix(60*DEGREES, OUT)[:2, :2]

        t_mat1 = MatrixVariable('T_1', np.eye(2))
        t_mat1.next_to(vector.formula, DOWN, aligned_edge=LEFT)

        t_mat2 = MatrixVariable('T_2', np.eye(2))
        t_mat2.next_to(t_mat1, DOWN, aligned_edge=LEFT)

        result = TrackerVectorWithFormulaAndArrow(
            coordinate_system_bg, vector.tracker.get_value(),
            label=r'T_2 T_1 \vec{v}',
            color=RED
        )

        result.formula.next_to(t_mat2, DOWN, aligned_edge=LEFT)
        result.add_updater(
            lambda r: r.tracker.set_value(t_mat2.tracker.get_value() @ t_mat1.tracker.get_value() @ vector.tracker.get_value())
        )

        self.play(
            Create(
                VGroup(
                    coordinate_system,
                    coordinate_system_bg,
                    vector,
                    t_mat1,
                    t_mat2,
                    result
                ),
                run_time=4
            )
        )

        self.play(ApplyMethod(t_mat1.tracker.set_value, t1, run_time=2), add_split=False)
        self.play(ApplyMethod(t_mat2.tracker.set_value, t2, run_time=2))

        self.set_header('But order matters!')
        self.play(ApplyMethod(t_mat1.tracker.set_value, np.eye(2), run_time=2), add_split=False)
        self.play(ApplyMethod(t_mat2.tracker.set_value, np.eye(2), run_time=2))

        self.play(ApplyMethod(t_mat1.tracker.set_value, t2, run_time=2), add_split=False)
        self.play(ApplyMethod(t_mat2.tracker.set_value, t1, run_time=2))

        self.clear()
        self.set_header('But order matters!')

        formula = MathTex(r'\vec{v}_{new} = A\enspace B\enspace \vec{v}',
                          substrings_to_isolate=['A\enspace', 'B\enspace']).scale(2)
        par_left = MathTex('(').scale(2)
        par_right = MathTex(')').scale(2)

        self.play(
            FadeIn(formula)
        )

        explain_text = SlideTextMarkdown('Transforms are applied **from right to left**', scale_factor=1)
        explain_text.next_to(formula, DOWN, buff=3*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        arrow_1 = CurvedArrow(formula[4].get_top(), formula[3].get_top())
        arrow_2 = CurvedArrow(formula[3].get_top(), formula[1].get_top())

        self.play(Write(explain_text), add_split=False)
        self.play(Create(arrow_1), add_split=False)
        self.play(Create(arrow_2))

        par_left.next_to(formula[3], LEFT, buff=0.1*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)
        par_right.next_to(formula[4], RIGHT, buff=0.1 * DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        new_explain_text = SlideTextMarkdown('But you can set parenthesis as you like!', scale_factor=2)
        new_explain_text.replace(explain_text)

        self.play(ReplacementTransform(explain_text, new_explain_text), add_split=False)
        self.play(Create(VGroup(par_left, par_right)))

        self.play(
            ApplyMethod(par_left.next_to, formula[1], LEFT, {'buff': 0.1*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER}),
            ApplyMethod(par_right.next_to, formula[3], RIGHT, {'buff': 0.1 * DEFAULT_MOBJECT_TO_MOBJECT_BUFFER})
        )

    def part16_identity_and_transpose(self):
        self.clear()
        self.set_header('The identity matrix')

        mat = MathTex(r'''
I = \begin{bmatrix}
1 & 0 & 0 & 0 & \dots & 0 \\
0 & 1 & 0 & 0 & \dots & 0 \\
0 & 0 & 1 & 0 & \dots & 0 \\
0 & 0 & 0 & 1 & \dots & 0 \\
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots \\
0 & 0 & 0 & 0 & \dots & 1 
\end{bmatrix}        
''')
        mat.to_edge(LEFT)

        text = SlideTextMarkdown(r'''
* If multiplied with a vector does not transform it
* The "neutral element" of matrix multiplication        
''', max_width='17em')
        text.next_to(mat, RIGHT, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)
        self.play(Write(mat),
                  Write(text))

        self.clear()
        self.set_header('Transpose')

        mat = np.array([[1, 2], [3, 4], [5, 6]])

        mat_object = MathTex('A', '=', a2l.to_ltx(mat, **self.a2l_kwargs))
        mat_object.shift(np.array([0, 1, 0]))
        mat_object_transposed = MathTex('A^T', '=', a2l.to_ltx(mat.T, **self.a2l_kwargs))
        mat_object_transposed.match_y(mat_object)

        self.play(
            Create(mat_object)
        )

        text = SlideTextMarkdown(r'''
* Rows become the columns
* Columns become the rows        
''', scale_factor=1)

        mat_group = Group(mat_object, mat_object_transposed)

        text.next_to(mat_object, DOWN, buff=3*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        def position_mat_group(m):
            m.arrange(center=False)
            m.set_x(0)

            return m

        self.play(ApplyFunction(position_mat_group, mat_group), add_split=False)
        self.play(
            Write(text)
        )

    def part17_inverse(self):
        self.clear()
        self.set_header('Inverse')

        coordinate_system = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg.fade(0.5)

        vector = TrackerVectorWithFormulaAndArrow(coordinate_system_bg,
                                                  np.array([2, 1]))

        vector.formula.next_to(self.sbdl_logo, DOWN, aligned_edge=LEFT)

        t_mat = MatrixVariable('T', np.eye(2))

        t_mat.next_to(vector.formula, DOWN, aligned_edge=LEFT)

        result = TrackerVectorWithFormulaAndArrow(
            coordinate_system_bg, vector.tracker.get_value(),
            label=r'T \vec{v}',
            color=RED
        )

        result.formula.next_to(t_mat, DOWN, aligned_edge=LEFT)

        result.add_updater(
            lambda r: r.tracker.set_value(t_mat.tracker.get_value() @ vector.tracker.get_value())
        )

        rot_mat = rotation_matrix(45*DEGREES, OUT)[:2, :2]

        coord_stuff = Group(
            coordinate_system,
            coordinate_system_bg,
            vector,
            t_mat,
            result
        )

        text = Tex(r"Let's say I do a transform like a rotation by 45\textdegree\\ on my original vector $\vec{v} = %s$:\\ \bigskip" % (vector.tracker.get_latex(), ),
                   r'$\vec{v}_{new} = %s %s$' % (a2l.to_ltx(rot_mat, **self.float_a2l_kwargs), vector.tracker.get_latex()))

        self.play(
            Write(text)
        )

        self.play(FadeOut(text), add_split=False)
        self.play(FadeIn(coord_stuff))

        self.play(
            ApplyMethod(t_mat.tracker.set_value, rot_mat)
        )

        self.set_header('Inverse')
        new_text = SlideText('The transformation matrix that "undoes" the original transformation is called the inverse.').center()

        self.play(
            FadeOut(coord_stuff),
            Write(new_text)
        )

        inv_rot = np.linalg.inv(rot_mat)

        self.play(
            AnimationGroup(
                FadeOut(new_text),
                FadeIn(coord_stuff)
            ), add_split=False)
        self.play(
            AnimationGroup(
                ApplyMethod(vector.tracker.set_value, result.tracker.get_value()),
                ApplyMethod(t_mat.tracker.set_value, np.eye(2))
            )
        )

        self.play(
            ApplyMethod(t_mat.tracker.set_value, inv_rot)
        )

        self.set_header('Properties of the Inverse')

        self.set_text(r'''
* The inverse of $A$ is $A^{-1}$
* $A A^{-1} = I$
* $A^{-1} A = I$
* $(A^{-1})^{-1} = A$
* $(A B)^{-1} = B^{-1} A^{-1}$       
''', scale_factor=1.2)

        self.play(
            FadeOut(coord_stuff),
            Write(self._slide_text)
        )

        self.set_header('Not all matrices have an inverse...')

        self.set_text(r'''
* The matrix must be square (i.e. $m=n$)
* The matrix must have "full rank":
    * Rank: Number of dimensions **after** the transform by the matrix
    * Must be equal to the number of columns
    * Remember that we want to "undo" the transform. If we collapse a 2D object to a line, we cannot undo this
''')

        self.wait()

        self.set_header('How to find the inverse')

        self.set_text(r'''
* If the matrix **only** rotates: $A^{-1} = A^T$
* If the matrix **only** scales:
    * This means, it is a diagonal matrix: $A = \begin{bmatrix}a & 0 & 0 \\\\ 0 & b & 0 \\\\ 0 & 0 & c \end{bmatrix}$
    * Take the inverse of each element: $A^{-1} = \begin{bmatrix}1/a & 0 & 0 \\\\ 0 & 1/b & 0 \\\\ 0 & 0 & 1/c \end{bmatrix}$        
''')

        self.wait()

        self.set_text(r'''
* In any other case, let a computer do this for you...
* The computer will most likely do a "factorization" of your matrix:
    * $A = BC$
    * Basically means that you can turn a complex transform into a series of rotations and scalings
    * And then we already know: $(B C)^{-1} = C^{-1} B^{-1}$
    * So: $A^{-1} = C^{-1} B^{-1}$       
''')

        self.wait()

    def part18_solve_linear_systems(self):
        self.clear()
        self.set_header('Linear equations')

        A = np.array([[9, 5, 4], [6, 3, -5], [3, -10, 6]])
        b = np.array([21, 7, 35])

        equation_ltx = []
        for a_row, cur_b in zip(A, b):
            this_str_list = []
            for idx, cur_el in enumerate(a_row):
                sign = ''
                if idx > 0 and cur_el >= 0:
                    sign = '+ '
                elif cur_el < 0:
                    sign = '- '

                this_str_list.append('%s%d x_%d' % (sign, np.abs(cur_el), idx+1))

            this_str = ' '.join(this_str_list)

            equation_ltx.append('%s &= %d' % (this_str, cur_b))


        equation_str = '\\\\ \n'.join(equation_ltx)

        equation_text = MathTex(equation_str)

        self.play(Write(equation_text))

        self.set_header('We can write this as a matrix multiplication')

        A_latex = '{{ ' + a2l.to_ltx(A, **self.a2l_kwargs) + ' }}'
        A_latex_inv = '{{ ' + a2l.to_ltx(A, **self.a2l_kwargs) + '^{-1} }}'
        A_latex_inv_numbers = '{{ ' + a2l.to_ltx(np.linalg.inv(A), **self.float_a2l_kwargs) + ' }}'
        b_latex = '{{ ' + a2l.to_ltx(b, **self.a2l_kwargs) + ' }}'

        mat_equation = MathTex(A_latex,
                               r'{{ \begin{bmatrix}x_1 \\ x_2 \\ x_3 \end{bmatrix} }}',
                               '=',
                               b_latex).scale(0.8)

        def position_equation(m):
            m.scale(0.5)
            m.to_corner()

            return m

        self.play(
            ApplyFunction(position_equation, equation_text),
            Write(mat_equation)
        )

        self.set_header('Now we can multiply both sides by $A^{-1}$')

        new_mat_equation = MathTex(A_latex_inv,
                                   A_latex,
                                   r'{{ \begin{bmatrix}x_1 \\ x_2 \\ x_3 \end{bmatrix} }}',
                                   '=',
                                   A_latex_inv,
                                   b_latex).scale(0.8)

        self.play(TransformMatchingTex(mat_equation, new_mat_equation))

        mat_equation = new_mat_equation

        self.set_header('Simplify...')

        new_mat_equation = MathTex('I',
                                   r'{{ \begin{bmatrix}x_1 \\ x_2 \\ x_3 \end{bmatrix} }}',
                                   '=',
                                   A_latex_inv_numbers,
                                   b_latex).scale(0.8)

        self.play(TransformMatchingTex(mat_equation, new_mat_equation))

        mat_equation = new_mat_equation

        new_mat_equation = MathTex(r'{{ \begin{bmatrix}x_1 \\ x_2 \\ x_3 \end{bmatrix} }}',
                                   '=',
                                   A_latex_inv_numbers,
                                   b_latex,
                                   '=',
                                   a2l.to_ltx(np.asarray(np.linalg.inv(A) @ b, dtype='int'), **self.a2l_kwargs)).scale(0.8)

        self.play(TransformMatchingTex(mat_equation, new_mat_equation))

    def part19_calculate_weighted_sums(self):
        self.clear()
        self.set_header('Calculate the weighted sum of $x$ using weights')

        meg_data_image = PNGImage(file_name='meg_data').scale_to_fit_height(4)
        meg_m_brace = Brace(meg_data_image, LEFT)
        meg_m_brace_text = MathTex('m = Number of channels').rotate(90*DEGREES).next_to(meg_m_brace, LEFT).scale(0.8)
        meg_n_brace = Brace(meg_data_image, DOWN)
        meg_n_brace_text = MathTex('n = Number of samples').next_to(meg_n_brace, DOWN).scale(0.8)

        self.play(
            FadeIn(meg_data_image),
            Create(
                VGroup(meg_m_brace, meg_m_brace_text,
                       meg_n_brace, meg_n_brace_text)
            )
        )

        meg_empty_mat = MathTex(self.create_empty_matrix(3, 6))
        meg_empty_mat_m_text = MathTex('Channels').rotate(90*DEGREES).next_to(meg_empty_mat, LEFT).scale(0.8)
        meg_empty_mat_n_text = MathTex('Samples').next_to(meg_empty_mat, DOWN).scale(0.8)

        meg_mat_group = VGroup(meg_empty_mat, meg_empty_mat_m_text, meg_empty_mat_n_text)

        meg_variable_tex = MathTex('x = ')

        meg_mat_group.next_to(meg_variable_tex, RIGHT)
        meg_variable_tex.match_y(meg_empty_mat_m_text)

        VGroup(meg_variable_tex, meg_mat_group).to_corner(DOWN + LEFT)

        brain_image = PNGImage(file_name='brain').scale_to_fit_height(3)
        brain_image.to_edge(RIGHT)
        brain_image.shift(np.array([0, -1, 0]))

        source_text = MathTex('s = w_1 s_1 + w_2 s_2 + \\dots')
        source_arrow = Arrow(brain_image.get_corner(UP+LEFT) + np.array([0, -0.5, 0]), brain_image.get_center())

        source_text.next_to(source_arrow, UP+LEFT)

        text = SlideTextMarkdown(
            'Gianpaolo is going to tell you soon that the activity at a source is the weighed sum of the sensor activity.'
        ).shift(np.array([0, -0.5, 0]))

        self.play(
            AnimationGroup(
                FadeOut(meg_data_image),
                Create(meg_variable_tex),
                ReplacementTransform(
                    VGroup(meg_m_brace, meg_m_brace_text, meg_n_brace, meg_n_brace_text),
                    meg_mat_group
                )
            ), add_split=False)
        self.play(
            AnimationGroup(
                FadeIn(brain_image),
                Write(text),
                Create(source_text),
                Create(source_arrow)
            )
        )

        new_text = SlideText("Don't mind now how to find these weights, but it boils down to:")
        new_text.center()
        new_text.set_y(text.get_y(UP), UP)


        w_empty_mat = MathTex(self.create_empty_matrix(5, 3))
        w_empty_mat_m_text = MathTex('Sources').rotate(90*DEGREES).next_to(w_empty_mat, LEFT).scale(0.8)
        w_empty_mat_n_text = MathTex('Channels').next_to(w_empty_mat, DOWN).scale(0.8)
        w_tex = SingleStringMathTex('W')

        w_mat_group = VGroup(w_empty_mat, w_empty_mat_m_text, w_empty_mat_n_text)

        source_variable_tex = MathTex('S =')
        source_variable_tex.match_x(meg_variable_tex)
        source_variable_tex.match_y(meg_variable_tex)

        new_megmat_group = meg_mat_group.copy()

        w_mat_group.next_to(source_variable_tex, RIGHT)
        new_megmat_group.next_to(w_mat_group, RIGHT)

        x_tex = SingleStringMathTex('x')
        x_tex.move_to(new_megmat_group[0].get_center())

        w_tex.move_to(w_empty_mat.get_center())

        source_variable_tex.match_y(w_tex)

        self.play(
            ReplacementTransform(text, new_text),
            FadeOut(source_text),
            FadeOut(source_arrow),
            ReplacementTransform(meg_variable_tex, source_variable_tex),
            ReplacementTransform(meg_mat_group, new_megmat_group),
            Create(w_mat_group),
            Create(w_tex),
            Create(x_tex)
        )

    def part20_covariance(self):
        self.clear()
        self.set_header('If you have a channels X samples data matrix, this is the covariance')

        self.set_text(r'$C = X X^T / n$', markdown=False, scale_factor=2)

        self._slide_text.center()

        self.wait()

        self.clear()
        self.set_header('Fun with covariance')

        coordinate_system = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg = BetterNumberPlane(**self.number_plane_config)
        coordinate_system_bg.fade(0.5)

        mixing_matrix = np.array([[0.8, 0.2], [-0.2, 0.8]])

        rng = np.random.default_rng(3)

        data_p1 = rng.standard_normal((2, 40)) * 2
        data_p1[1, :] *= 0.15

        data_p2 = rng.standard_normal((2, 40)) * 2
        data_p2[0, :] *= 0.2

        all_data = np.hstack((data_p1, data_p2))

        all_data_projected = mixing_matrix @ all_data

        all_dots = list()

        for cur_coord in all_data_projected.T:
            all_dots.append(Dot(coordinate_system.c2p(*cur_coord)))

        self.play(
            AnimationGroup(
                Create(coordinate_system),
                Create(coordinate_system_bg)
            ), add_split=False)
        self.play(Create(VGroup(*all_dots), run_time=5))

        cov = all_data_projected @ all_data_projected.T / all_data_projected.shape[1]

        eig_vals, eig_vectors = np.linalg.eig(cov)

        eig_angles = np.mod([np.angle(complex(*e)) for e in eig_vectors.T], 2*np.pi)
        eig_angles.sort()

        cov_variable = MatrixVariable(label='cov', initial_value=cov, color=RED)
        cov_variable.next_to(self.sbdl_logo, DOWN, aligned_edge=LEFT)

        self.play(
            Create(cov_variable)
        )

        v_1 = coordinate_system.get_vector(cov[:, 0], color=RED)
        v_2 = coordinate_system.get_vector(cov[:, 1], color=RED)

        self.play(Create(v_1), add_split=False)
        self.play(Create(v_2))

        self.set_header('Let\'s transform with cov...')

        probe_vec = TrackerVectorWithFormulaAndArrow(coordinate_system,
                                                     np.array([1, 0]), label=r'\vec{x}',
                                                     arrow_color=WHITE, color=WHITE)

        result_vec = TrackerVectorWithFormulaAndArrow(coordinate_system,
                                                      np.array([1, 0]),
                                                      label=r'cov \cdot \vec{x}',
                                                      arrow_color=GREEN, color=GREEN)

        result_vec.add_updater(
            lambda r: r.tracker.set_value(cov @ probe_vec.tracker.get_value())
        )

        probe_angle_tracker = ValueTracker(0)
        probe_vec.add_updater(
            lambda p: p.tracker.set_value(rotation_matrix(probe_angle_tracker.get_value(), OUT)[:2, :2] @ np.array([1, 0]))
        )

        probe_vec.formula.next_to(cov_variable, DOWN, aligned_edge=LEFT)
        result_vec.formula.next_to(probe_vec.formula, DOWN, aligned_edge=LEFT)

        self.play(FadeOut(VGroup(*all_dots)), add_split=False)
        self.play(Create(probe_vec), add_split=False)
        self.play(Create(result_vec))

        eig_vectors_objects = [coordinate_system.get_vector(eval * evec, color=BLUE) for evec, eval in zip(eig_vectors.T, eig_vals)]

        for cur_angle, cur_evec in zip(eig_angles, eig_vectors_objects[::-1]):
            self.play(
                ApplyMethod(probe_angle_tracker.set_value, cur_angle, run_time=5),
                add_split=False)

            self.play(Create(cur_evec))

        self.foreground_mobjects.extend(eig_vectors_objects)

        self.play(
            FadeIn(VGroup(*all_dots)),
            FadeOut(VGroup(
                cov_variable,
                result_vec,
                probe_vec,
                v_1,
                v_2
            )),
            add_split=False
        )

        text1 = SlideText(r'''
These two vectors are special: When they are transformed by the covariance matrix, they \textbf{only} change the length \textbf{not} the direction! \\
They are the solution for this equation: $A\vec{x} = \lambda \vec{x}$         
''', scale_factor=1, max_width='25em')

        text1.next_to(self._slide_header, DOWN, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)
        bg_rect1 = BackgroundRectangle(text1)

        self.play(
            FadeIn(Group(bg_rect1, text1))
        )

        text2 = SlideText('These are the principle components! \\ '
                          'And the length of the arrow represents the explained variance.', scale_factor=1, max_width='25em')

        text2.next_to(self._slide_header, DOWN, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)
        bg_rect2 = BackgroundRectangle(text2)

        self.play(
            FadeOut(Group(bg_rect1, text1)),
            FadeIn(Group(bg_rect2, text2))
        )

        eig_vect_variable = MatrixVariable('eig^T', eig_vectors.T)
        eig_vect_variable.next_to(self.sbdl_logo, DOWN, aligned_edge=LEFT)
        cov_variable.next_to(eig_vect_variable, DOWN, aligned_edge=LEFT)

        self.set_header('This is "Whitening"')

        self.play(
            FadeOut(Group(bg_rect2, text2)),
            Create(eig_vect_variable),
            Create(cov_variable)
        )

        def update_cov(t):
            new_data_mat = eig_vectors.T @ all_data_projected
            new_cov = new_data_mat @ new_data_mat.T / new_data_mat.shape[1]

            t.set_value(new_cov)

            return t

        self.play(
            ApplyMethod(VGroup(*all_dots).apply_matrix, eig_vectors.T, {'about_point': coordinate_system.c2p(0, 0)}),
            ApplyFunction(update_cov, cov_variable.tracker)
        )


    def construct(self):
        self.part01_title_page()
        self.part02_overview()
        self.part03_introduce_hamilton()
        self.part04_introduce_vectors()
        self.part05_3d_vectors()
        self.part06_about_dimensions()
        self.part07_basic_vector_operations()
        self.part08_the_dot_product()
        self.part09_the_dot_product_part2()
        self.part10_base_vectors()
        self.part12_linear_dependence()
        self.part13_introduce_matrix_multiplication()
        self.part14_more_matmult()
        self.part15_chain_transformtations()
        self.part16_identity_and_transpose()
        self.part17_inverse()
        self.part18_solve_linear_systems()
        self.part19_calculate_weighted_sums()
        self.part20_covariance()

    def _update_matrix_part(self, o, tex_idx, mat, scale=1,
                            a2l_kwargs=None):
        if a2l_kwargs is None:
            a2l_kwargs = self.a2l_kwargs

        if isinstance(mat, np.ndarray):
            mat = a2l.to_ltx(mat, **a2l_kwargs)

        new_tex = SingleStringMathTex(mat).scale(scale)

        tex_o = o
        if isinstance(o, MathTexWithOpaqueBackground):
            tex_o = o.tex

        old_mo = tex_o.submobjects[tex_idx]
        new_tex.align_to(old_mo, LEFT)
        new_tex.set_y(old_mo.get_y())
        new_tex.match_style(old_mo)

        old_family = tex_o.get_family()
        tex_o.submobjects[tex_idx] = new_tex

        if old_mo in old_family:
            old_mo.points[:] = 0
            for m in old_mo.get_family():
                m.points[:] = 0
            self.remove(*old_mo.get_family())
            old_mo.submobjects = []

        if isinstance(o, MathTexWithOpaqueBackground):
            o.update_rect()

    def _reposition_arrow(self, arrow, start, end):
        arrow.start = start
        arrow.end = end

        if not np.all(start == end):
            #arrow.pop_tips()
            arrow.set_points_as_corners([start, end])
            arrow.put_start_and_end_on(start, end)
            arrow.reset_points()
            arrow.generate_points()
            arrow.position_tip(arrow.tip)
            #arrow.add_tip()

    def create_empty_matrix(self, m, n):
        ltx_str = r'\begin{bmatrix}'

        for cm in range(m):
            for cn in range(n):
                ltx_str += r' & '

            ltx_str += r' \\'

        ltx_str += r'\end{bmatrix}'

        return ltx_str

    def create_labeled_empty_mat(self, m, n, add_to_text='', add_to_text2=None):
        if add_to_text2 is None:
            add_to_text2 = add_to_text
        mat_obj = MathTex(self.create_empty_matrix(m, n))
        mat_m_text = MathTex('m%s = %d' % (add_to_text, m)).rotate(90*DEGREES)
        mat_n_text = MathTex('n%s = %d' % (add_to_text2, n))

        mat_m_text.next_to(mat_obj, LEFT)
        mat_n_text.next_to(mat_obj, DOWN)

        return VGroup(mat_obj, mat_m_text, mat_n_text)


class SVD(LinearAlgebra):
    default_add_split_at_play = False
    def construct(self):
        self.clear()
        self.set_header('Enjoy the power of the SVD')

        image_raw = PIL.Image.open('assets/eileen_collins.jpg')
        image_raw.thumbnail((500, 500))
        image_grayscale_raw = image_raw.convert('L')
        image_m = image_raw.height
        image_n = image_raw.width

        original_image = ImageMobject(image_raw)
        original_image.scale(self.scale_astronaut)

        grayscale_image = ImageMobject(image_grayscale_raw)
        grayscale_image.scale(self.scale_astronaut)

        self.play(FadeIn(original_image))
        self.wait(1)

        self.play(Transform(original_image, grayscale_image,
                            replace_mobject_with_target_in_scene=True))

        self.wait(1)

        zoom_rect = Rectangle(color=RED,
                              stroke_width=DEFAULT_STROKE_WIDTH * 2,
                              height=1,
                              width=1.5)
        zoom_rect.shift(RIGHT * 3, UP * 1)
        self.add(zoom_rect)
        self.wait()
        grayscale_image.set_z_index(-1)

        scale_factor = 0.1

        image_grayscale_raw_cropped = image_grayscale_raw.crop((0, 0, *self.zoomed_dim))
        self.play(ApplyFunction(lambda r: self.zoom_rect(r, scale_factor, grayscale_image), zoom_rect))
        grayscale_image_zoomed = ImageMobject(
            image_grayscale_raw_cropped.resize((image_grayscale_raw.width, image_grayscale_raw.height),
                                               resample=PIL.Image.NEAREST))
        self.add(grayscale_image_zoomed)
        self.match_a_to_b(grayscale_image_zoomed, zoom_rect)

        self.play(AnimationGroup(
            ApplyFunction(lambda r: self.match_a_to_b(r,
                                                      grayscale_image),
                          zoom_rect),
            ApplyFunction(lambda r: self.match_a_to_b(r,
                                                      grayscale_image),
                          grayscale_image_zoomed),
            FadeOut(grayscale_image)
        ))

        zoomed_matrix = Matrix(np.asarray(image_grayscale_raw_cropped))
        zoomed_image_group = Group(grayscale_image_zoomed, zoom_rect)
        zoomed_super_group = Group(zoomed_matrix, zoomed_image_group)
        self.add(zoomed_matrix)

        self.play(AnimationGroup(
            FadeIn(zoomed_matrix, target_position=LEFT, shift=RIGHT),
            ApplyMethod(zoomed_super_group.arrange, LEFT, buff=2),
        ))

        self.wait()
        original_mat_width = grayscale_image.get_width()
        original_mat_height = grayscale_image.get_height()
        rect_matrix = MatrixAsRect(np.asarray(image_grayscale_raw),
                                   width=original_mat_width,
                                   height=original_mat_height)

        Group(grayscale_image, rect_matrix).arrange(RIGHT, buff=2)

        self.play(AnimationGroup(
            ApplyMethod(grayscale_image_zoomed.scale, 0, about_edge=UL),
            ApplyMethod(zoom_rect.scale, 0, about_edge=UL),
            ApplyMethod(zoomed_matrix.scale, 0, about_edge=UL),
            GrowFromPoint(grayscale_image, zoomed_image_group.get_corner(UL)),
            GrowFromPoint(rect_matrix, zoomed_matrix.get_corner(UL)),
        ))

        grayscale_braces = Group(
            BraceLabel(grayscale_image, image_n),
            BraceLabel(grayscale_image, image_m, LEFT)
        )

        self.wait()

        a_text = MathTex('A =')
        a_text.scale(2)
        a_text.align_to(grayscale_image, RIGHT)

        self.play(AnimationGroup(
            ApplyFunction(
                lambda x: x.scale(0.4, about_edge=UL).to_corner(UL),
                Group(grayscale_image, grayscale_braces)
            ),
            Create(a_text)
        ))

        self.wait()

        rect_formular_group_left = Group(
            MathTex('=').scale(2)
        )

        scale_mat_to_formula = 0.3

        self.play(ApplyMethod(a_text.shift, DOWN))
        self.play(AnimationGroup(
            ApplyFunction(
                lambda x: x.scale(scale_mat_to_formula).next_to(a_text.submobjects[0][0], UP, aligned_edge=RIGHT,
                                                                buff=0.6).update_braces(0.5),
                rect_matrix
            )
        ))

        rect_formular_group_left.next_to(rect_matrix, index_of_submobject_to_align=0).match_x(
            a_text.submobjects[0][1])

        self.play(AnimationGroup(
            FadeIn(rect_formular_group_left),
        ))

        rect_formular_group_left.add(rect_matrix)

        self.wait()

        u, s, v = np.linalg.svd(image_grayscale_raw, True)

        full_text_formula = MathTex('A = U * S * V^T').scale(2)
        full_text_formula.align_to(a_text, DL)

        self.play(AnimationGroup(
            ReplacementTransform(a_text, full_text_formula)
        ))

        u_rect = MatrixAsRect(u, height=original_mat_height).scale(scale_mat_to_formula).update_braces(0.5)
        s_rect = MatrixAsRect(s, height=original_mat_height).scale(scale_mat_to_formula).update_braces(0.5)
        vt_rect = MatrixAsRect(v.T, height=original_mat_height).scale(scale_mat_to_formula).update_braces(0.5)

        rect_formula_right = Group(u_rect,
                                   MathTex('* I').scale(2),
                                   s_rect,
                                   MathTex('*').scale(2),
                                   vt_rect)

        rect_formula_right.arrange(index_of_submobject_to_align=0, coor_mask=[0, 1, 0])
        rect_formula_right.arrange(coor_mask=[1, 0, 0])

        rect_formula_right.next_to(rect_formular_group_left)

        self.play(FadeIn(rect_formula_right))

        self.wait()

        self.play(AnimationGroup(
            FadeOut(full_text_formula, target_position=DOWN, shift=DOWN),
            FadeOut(grayscale_braces),
            FadeOut(rect_matrix, target_position=DOWN, shift=DOWN),
            ApplyFunction(
                lambda x: x.scale(2).next_to(rect_formular_group_left.submobjects[0], LEFT),
                grayscale_image
            )
        ))

        self.remove(rect_matrix)
        rect_formular_group_left.remove(rect_matrix)

        staging_pic = grayscale_image.copy()
        staging_pic.pixel_array = np.zeros_like(staging_pic.get_pixel_array(), dtype='uint8')
        staging_pic.pixel_array[:, :, 3] = 255
        staging_pic.next_to(grayscale_image, DOWN)
        staging_equal = MathTex('=').scale(2)
        staging_equal.next_to(staging_pic)
        staging_equal.set_x(rect_formular_group_left.submobjects[0].get_x())

        tmp_u = np.zeros_like(u[:, 0]) + 255
        tmp_v = np.zeros_like(v[0, :]) + 255

        staging_u_vector = ImageFromVector(tmp_u, scale_factor_width=self.scale_factor_width_vectors * 2)
        staging_v_vector = ImageFromVector(tmp_v, transpose=True,
                                           scale_factor_width=self.scale_factor_width_vectors * 2)
        staging_times = MathTex('*').scale(2)

        staging_u_vector.next_to(staging_equal)
        staging_times.next_to(staging_u_vector)
        staging_v_vector.next_to(staging_times)

        staging_u_vector.set_x(u_rect._rect.get_x(LEFT))
        staging_times.set_x(rect_formula_right.submobjects[1][0].get_x(LEFT), LEFT)
        staging_v_vector.set_y(staging_u_vector.get_y(UP), UP)
        staging_v_vector.set_x(
            staging_times.get_x(LEFT) + (staging_times.get_x(LEFT) - staging_u_vector.get_x(LEFT)),
            LEFT)

        self.add(staging_equal, staging_pic, staging_u_vector, staging_v_vector, staging_times)

        staging_group = Group(staging_pic, staging_equal, staging_u_vector, staging_v_vector, staging_times)

        self.play(ApplyMethod(
            Group(grayscale_image, rect_formular_group_left, rect_formula_right, staging_group).center
        ))

        self.wait()

        new_image = grayscale_image.copy()
        new_image_pixels = np.zeros_like(new_image.get_pixel_array(), dtype='float64')
        new_image_pixels[:, :, 3] = 255
        new_image.pixel_array = new_image_pixels.astype('uint8')

        self.play(ReplacementTransform(grayscale_image, new_image))
        self.wait()

        n_component_var = Variable(1, 'n_components', var_type=Integer)
        n_component_var.next_to(self._slide_number_object, LEFT, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        self.add(n_component_var)

        for n_component in range(200):
            if n_component < 10:
                run_time = 1
            elif n_component < 30:
                run_time = 0.5
            elif n_component < 60:
                run_time = 0.1
            else:
                run_time = 0.01

            cur_u = u[:, n_component]
            cur_s = s[n_component]
            cur_v = v[n_component, :]

            cur_u_image = ImageFromVectorAlignedToMatrixAsRect(cur_u * cur_s, u_rect,
                                                               scale_factor_width=self.scale_factor_width_vectors,
                                                               offset=n_component)

            cur_v_image = ImageFromVectorAlignedToMatrixAsRect(cur_v * cur_s, vt_rect, transpose=True,
                                                               scale_factor_width=self.scale_factor_width_vectors,
                                                               offset=n_component)

            new_image_mat = np.outer(cur_u * cur_s, cur_v)
            new_image_mat = new_image_mat[:, :, np.newaxis].repeat(3, axis=2)

            new_image = grayscale_image.copy()
            new_image_pixels[:, :, 0:3] += new_image_mat
            new_image.pixel_array = new_image_pixels.astype('uint8')

            new_staging_image = staging_pic.copy()
            new_staging_image.pixel_array[:, :, :3] = new_image_mat.astype('uint8')

            if n_component < 30:
                self.play(
                    ApplyMethod(n_component_var.tracker.set_value, n_component+1, run_time=run_time),
                    FadeIn(Group(cur_v_image, cur_u_image), target_position=UP, shift=DOWN, run_time=run_time),
                    add_split=False)
                self.play(
                    AnimationGroup(
                        ApplyMethod(cur_u_image.replace, staging_u_vector, run_time=run_time),
                        ApplyMethod(cur_v_image.replace, staging_v_vector, run_time=run_time),
                    )
                )
                self.play(ReplacementTransform(staging_pic, new_staging_image, run_time=run_time), add_split=False)
                self.play(ApplyMethod(new_staging_image.align_to, grayscale_image, run_time=run_time), add_split=False)
                self.play(
                    AnimationGroup(
                        FadeOut(new_staging_image, run_time=run_time),
                        FadeOut(grayscale_image, run_time=run_time),
                        FadeIn(new_image, run_time=run_time)
                    )
                )
            else:
                self.play(
                    ApplyMethod(n_component_var.tracker.set_value, n_component + 1, run_time=run_time),
                    FadeOut(grayscale_image, run_time=0.1),
                    FadeIn(new_image, run_time=0.1)
                )

            grayscale_image = new_image

    def zoom_rect(self, rect, scale_factor, grayscale_image):
        rect.points *= scale_factor
        rect.align_to(grayscale_image, UL)
        return rect

    def match_a_to_b(self, a, b):
        a.stretch_to_fit_height(b.height)
        a.stretch_to_fit_width(b.width)
        a.align_to(b, UL)

        return a
